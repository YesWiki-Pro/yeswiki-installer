# ----------------------------------------------------------------------
# | Config file for <?=$this->e($domain)?> host                                    |
# ----------------------------------------------------------------------

server {
  listen [::]:80;
  listen 80;
  server_name <?php if (!$subDomain) : ?>www.<?=$this->e($domain)?> <?php endif ?><?=$this->e($domain)?>;
  
  location ^~ /.well-known/acme-challenge/ {
      root /home/<?=$this->e($user)?>/<?=$this->e($domain)?>;
      try_files $uri =404;
  } 

  location / {
      return 301 https://<?=$this->e($domain)?>$request_uri;
  }
}

<?php if (!$subDomain) : ?>
server {
  listen [::]:443 ssl http2;
  listen 443 ssl http2;

  server_name www.<?=$this->e($domain)?>;

  include h5bp/tls/ssl_engine.conf;
  ssl_certificate /etc/letsencrypt/live/<?=$this->e($domain)?>/fullchain.pem;
  ssl_certificate_key /etc/letsencrypt/live/<?=$this->e($domain)?>/key.pem;
  ssl_trusted_certificate /etc/letsencrypt/live/<?=$this->e($domain)?>/ca.pem;
  include h5bp/tls/policy_strict.conf;

  return 301 $scheme://<?=$this->e($domain)?>$request_uri;
}
<?php endif ?>

server {
  listen [::]:443 ssl http2;
  listen 443 ssl http2;

  # The host name to respond to
  server_name <?=$this->e($domain)?>;

  include h5bp/tls/ssl_engine.conf;
  <?php if ($subDomain) : ?>
  include h5bp/tls/certificate_files.conf;
  <?php else : ?>
  ssl_certificate /etc/letsencrypt/live/<?=$this->e($domain)?>/fullchain.pem;
  ssl_certificate_key /etc/letsencrypt/live/<?=$this->e($domain)?>/key.pem;
  ssl_trusted_certificate /etc/letsencrypt/live/<?=$this->e($domain)?>/ca.pem;
  <?php endif ?>
  include h5bp/tls/policy_strict.conf;

  # Path for static files
  root /home/<?=$this->e($user)?>/<?=$this->e($domain)?>;

  # Custom error pages
  include h5bp/errors/custom_errors.conf;

  # Include the basic h5bp config set
  include h5bp/basic.conf;
  
  access_log /var/log/nginx/<?=$this->e($domain)?>-access.log;
  error_log  /var/log/nginx/<?=$this->e($domain)?>-error.log error;
  index index.php index.html index.htm;

  location / {
    <?php if (!empty($herseUser) && !empty($hersePass)) : ?>
    auth_basic "Accès restreint";
    auth_basic_user_file /home/<?=$this->e($user)?>/<?=$this->e($domain)?>/.htpasswd;
    <?php endif ?>
    try_files $uri $uri/ /index.php$is_args$args;
  }
  
  # private folder for YesWiki
  location ~* /(.*/)?private/ {
    deny all;
    return 403;
  }

  location ~ \.php$ {
    fastcgi_split_path_info ^(.+\.php)(/.+)$;
    fastcgi_pass unix:/var/run/php-fpm-<?=$this->e($user)?>.sock;
    fastcgi_index index.php;
    include fastcgi.conf;
  }
}

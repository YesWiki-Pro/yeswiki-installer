# ----------------------------------------------------------------------
# | Config file for <?=$this->e($domain)?> host                                    |
# ----------------------------------------------------------------------

server {
  listen [::]:80;
  listen 80;
  server_name <?php if (!$subDomain) : ?>www.<?=$this->e($domain)?> <?php endif ?><?=$this->e($domain)?>;
  
  location ^~ /.well-known/acme-challenge/ {
      root /home/<?=$this->e($user)?>/<?=$this->e($domain)?>;
      try_files $uri =404;
  } 

  # Path for static files
  root /home/<?=$this->e($user)?>/<?=$this->e($domain)?>;

  # Custom error pages
  include h5bp/errors/custom_errors.conf;

  # Include the basic h5bp config set
  include h5bp/basic.conf;
  
  access_log /var/log/nginx/<?=$this->e($domain)?>-access.log;
  error_log  /var/log/nginx/<?=$this->e($domain)?>-error.log error;
  index index.php index.html index.htm;

  location / {
    <?php if (!empty($herseUser) && !empty($hersePass)) : ?>
    auth_basic "Accès restreint";
    auth_basic_user_file /home/<?=$this->e($user)?>/<?=$this->e($domain)?>/.htpasswd;
    <?php endif ?>
    try_files $uri $uri/ /index.php$is_args$args;
  }

  # private folder for YesWiki
  location ~* /(.*/)?private/ {
    deny all;
    return 403;
  }

  location ~ \.php$ {
    fastcgi_split_path_info ^(.+\.php)(/.+)$;
    fastcgi_pass unix:/var/run/php-fpm-<?=$this->e($user)?>.sock;
    fastcgi_index index.php;
    include fastcgi.conf;
  }
}

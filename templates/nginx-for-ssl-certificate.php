# ----------------------------------------------------------------------
# | Config file for ssl certificate for <?=$this->e($domain)?> host    |
# ----------------------------------------------------------------------

server {
  listen [::]:80;
  listen 80;
  server_name www.<?=$this->e($domain)?> <?=$this->e($domain)?>;
  location ^~ /.well-known/acme-challenge/ {
      root /home/<?=$this->e($user)?>/<?=$this->e($domain)?>;
      try_files $uri =404;
  }
}

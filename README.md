# yeswiki-installer

Installer script for YesWiki, working on an opiniated Debian 11 environment.

## Setup

- clone this repository on your server
- go in the folder `cd yeswiki-installer`
- copy .env.example to .env `cp .env.example .env`
- edit .env file and change it with your informations
- add executable rights to some files `chmod +x yeswiki-installer.php yeswiki-remover.php`
- make symlinks if you want to use those commands globally and more easily
```
sudo ln -s yeswiki.installer.php /usr/local/bin/yeswiki-installer
sudo ln -s yeswiki.remover.php /usr/local/bin/yeswiki-remover
```

## Usage

**You need root acces to use those commands**

Type `sudo yeswiki-installer` to see all the options for installation of YesWiki.

Type `sudo yeswiki-remover` to see all the options for removal of YesWiki
<?php

require 'vendor/autoload.php';

function checkIP($domain, $withWww = false, $noip6 = false)
{
    if (!preg_match('/(?=^.{4,253}$)(^((?!-)[a-zA-Z0-9-]{0,62}[a-zA-Z0-9]\.)+[a-zA-Z]{2,63}$)/', strtolower($domain))) {
        throw new Exception('not valid domain : "'.$domain.'".');
    }
    $output = shell_exec('ping -c1 -4 '.$domain);
    preg_match_all('/PING.*\(.*((\b25[0-5]|\b2[0-4][0-9]|\b[01]?[0-9][0-9]?)(\.(25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)){3}).*\)/m', $output, $matches, PREG_SET_ORDER, 0);
    $currentip = $matches[0][1] ?? null;
    if (empty($currentip)) {
        throw new Exception('the domain '.$domain.' has no ip v4.');
    }
    if ($currentip !== $_SERVER['ip4']) {
        throw new Exception('the current ip v4 address of '.$domain.' is '.$currentip.'. it should be '.$_SERVER['ip4']);
    }
    if (!$noip6) {
        $output = shell_exec('ping -c1 -6 '.$domain);
        preg_match_all('/PING.*\((([0-9a-fA-F]{1,4}:){7,7}[0-9a-fA-F]{1,4}|([0-9a-fA-F]{1,4}:){1,7}:|([0-9a-fA-F]{1,4}:){1,6}:[0-9a-fA-F]{1,4}|([0-9a-fA-F]{1,4}:){1,5}(:[0-9a-fA-F]{1,4}){1,2}|([0-9a-fA-F]{1,4}:){1,4}(:[0-9a-fA-F]{1,4}){1,3}|([0-9a-fA-F]{1,4}:){1,3}(:[0-9a-fA-F]{1,4}){1,4}|([0-9a-fA-F]{1,4}:){1,2}(:[0-9a-fA-F]{1,4}){1,5}|[0-9a-fA-F]{1,4}:((:[0-9a-fA-F]{1,4}){1,6})|:((:[0-9a-fA-F]{1,4}){1,7}|:)|fe80:(:[0-9a-fA-F]{0,4}){0,4}%[0-9a-zA-Z]{1,}|::(ffff(:0{1,4}){0,1}:){0,1}((25[0-5]|(2[0-4]|1{0,1}[0-9]){0,1}[0-9])\.){3,3}(25[0-5]|(2[0-4]|1{0,1}[0-9]){0,1}[0-9])|([0-9a-fA-F]{1,4}:){1,4}:((25[0-5]|(2[0-4]|1{0,1}[0-9]){0,1}[0-9])\.){3,3}(25[0-5]|(2[0-4]|1{0,1}[0-9]){0,1}[0-9]))\)/m', $output, $matches, PREG_SET_ORDER, 0);
        $currentip6 = $matches[0][1] ?? null;
        if (empty($currentip6)) {
            throw new Exception('the domain '.$domain.' has no ip v6.');
        }
        if ($currentip6 !== $_SERVER['ip6']) {
            throw new Exception('the current ip v6 address of '.$domain.' is '.$currentip6.'. it should be '.$_SERVER['ip6']);
        }
    }
    if ($withWww) {
        $dnsentries = dns_get_record('www.'.$domain, DNS_A + DNS_AAAA + DNS_CNAME);
        $foundCnameEntry = false;
        $foundIp4 = false;
        $foundIp6 = false;
        foreach($dnsentries as $key => $row) {
            if ($row['host'] == 'www.'.$domain && $row['type']=='CNAME' && $row['target'] == $domain) {
                $foundCnameEntry = true;
            }
            if ($row['host'] == 'www.'.$domain && $row['type']=='A' && $row['ip'] == $_SERVER['ip4']) {
                $foundIp4 = true;
            }
            if (!$noip6) {
                if ($row['host'] == 'www.'.$domain && $row['type']=='AAAA' && $row['ipv6'] == $_SERVER['ip6']) {
                    $foundIp6 = true;
                }
            }
        }
        if (!$foundCnameEntry) {
            if (!$foundIp4) {
                throw new Exception('the domain www.'.$domain.' was not found in DNS record as a CNAME targeting '.$domain."\n".' or A entry to '.$_SERVER['ip4']);
            }
            if (!$noip6 && !$foundIp6) {
                throw new Exception('the domain www.'.$domain.' was not found in DNS record as a CNAME targeting '.$domain."\n".' or AAAA entry to '.$_SERVER['ip6'].'.');
            }
        }
    }
    return true;
}

function checkIfInstalled($domain)
{
    exec('find /home/* -maxdepth 1 -type d | grep '.$domain, $output);
    if (!empty($output)) {
        throw new Exception('the domain '.$domain.' was already found on the server.');
    }
}

function checkIfUserExist($user)
{
    exec('cut -d: -f1 /etc/passwd | grep '.$user, $output);
    return !empty($output);
}

function generateUserFromDomain($domain, $recursive = null)
{
    if ($recursive == 100) {
        throw new Exception('Too much users found, 100 that is too much for '.$domain);
    }
    $user = str_split(str_replace([$_SERVER['maindomain'], '-', '.'], '', $domain), 30)[0].$recursive;
    // try anthor username if user exists or if reserved name
    if (checkIfUserExist($user) || in_array($user, explode(',', $_SERVER['reservedsubdomains']))) {
        if ($recursive === null) {
            $recursive = 1;
        }
        $user = generateUserFromDomain($domain, $recursive + 1);
    }
    return $user;
}

function findUserFromExistingDomain($domain)
{
    exec('find /home/* -maxdepth 1 -type d | grep '.$domain, $output);
    if (empty($output)) {
        throw new Exception('the domain '.$domain.' was not found on the server.');
    } else {
        return str_replace(['/home/', '/'.$domain], '', $output[0]);
    }
}

function generatePassword($length = 32, $add_dashes = false, $available_sets = 'luds')
{
    $sets = array();
    if (strpos($available_sets, 'l') !== false) {
        $sets[] = 'abcdefghjkmnpqrstuvwxyz';
    }
    if (strpos($available_sets, 'u') !== false) {
        $sets[] = 'ABCDEFGHJKMNPQRSTUVWXYZ';
    }
    if (strpos($available_sets, 'd') !== false) {
        $sets[] = '23456789';
    }
    if (strpos($available_sets, 's') !== false) {
        $sets[] = ';-().!?';
    }

    $all = '';
    $password = '';
    foreach ($sets as $set) {
        $password .= $set[array_rand(str_split($set))];
        $all .= $set;
    }

    $all = str_split($all);
    for ($i = 0; $i < $length - count($sets); $i++) {
        $password .= $all[array_rand($all)];
    }

    $password = str_shuffle($password);

    if (!$add_dashes) {
        return $password;
    }

    $dash_len = floor(sqrt($length));
    $dash_str = '';
    while (strlen($password) > $dash_len) {
        $dash_str .= substr($password, 0, $dash_len) . '-';
        $password = substr($password, $dash_len);
    }
    $dash_str .= $password;
    return $dash_str;
}

function createSQLUserAndDatabase($user, $type)
{
    $pass = generatePassword();
    exec('mysql -u '.$_SERVER['mysqluser'].' -p\''.$_SERVER['mysqlpassword'].'\' -e \'CREATE DATABASE IF NOT EXISTS '.$user.';\'', $output);
    exec('mysql -u '.$_SERVER['mysqluser'].' -p\''.$_SERVER['mysqlpassword'].'\' -e "CREATE USER IF NOT EXISTS \''.$user.'\'@\'localhost\' IDENTIFIED BY \''.$pass.'\';"', $output);
    exec('mysql -u '.$_SERVER['mysqluser'].' -p\''.$_SERVER['mysqlpassword'].'\' -e "GRANT ALL PRIVILEGES ON '.$user.'.* TO \''.$user.'\'@\'localhost\';"', $output);
    exec('mysql -u '.$_SERVER['mysqluser'].' -p\''.$_SERVER['mysqlpassword'].'\' -e "FLUSH PRIVILEGES;"', $output);
    $databaseModel = ($type === 'solo') ? $_SERVER['solomodel'] : $_SERVER['fermemodel'];
    exec('mysqldump -u '.$_SERVER['mysqluser'].' -p\''.$_SERVER['mysqlpassword'].'\' -v '.$databaseModel.' | mysql -u '.$user.' -p\''.$pass.'\' -D '.$user, $output);
    // TODO: add first user and make him admin
    return ['database' => $user, 'user' => $user, 'password' => $pass];
}

function removeMySQLUserAndDatabase($user)
{
    exec('mysql -u '.$_SERVER['mysqluser'].' -p\''.$_SERVER['mysqlpassword'].'\' -e \'DROP DATABASE IF EXISTS '.$user.';\'', $output);
    exec('mysql -u '.$_SERVER['mysqluser'].' -p\''.$_SERVER['mysqlpassword'].'\' -e "DROP USER IF EXISTS \''.$user.'\'@\'localhost\';"', $output);
    exec('mysql -u '.$_SERVER['mysqluser'].' -p\''.$_SERVER['mysqlpassword'].'\' -e "FLUSH PRIVILEGES;"', $output);
    return;
}

function createUnixUserWithQuota($user, $quota)
{
    $pass = generatePassword();
    exec('useradd -m -p "'.$pass.'" '.$user, $output);
    exec('setquota -u '.$user.' '.$quota.' '.$quota.' 0 0 -a /dev/loop0', $output);
    // TODO : handle errors
    return ['user' => $user, 'password' => $pass, 'quota' => $quota];
}

function removeUnixUser($user)
{
    exec('deluser --remove-home '.$user, $output);
    // TODO : handle errors
    return;
}

function createNginxConfig($domain, $user, $herseUser, $hersePass, $nossl)
{
    // create folder if not exists
    exec('sudo -u '.$user.' mkdir -p /home'.'/'.$user.'/'.$domain);
    
    $nginxFile = '/etc/nginx/conf.d/'.$domain.'.conf';
    if (empty($herseUser) && empty($hersePass)) {
        // no herse needed
    } elseif (empty($herseUser) || empty($hersePass)) {
        throw new Exception('You need an username AND a password to add a herse.');
    } else {
        // add password file to domain
        file_put_contents(
            '/home'.'/'.$user.'/'.$domain.'/.htpasswd',
            $herseUser.':'.password_hash($hersePass, PASSWORD_BCRYPT)
        );
    }
    $templates = new League\Plates\Engine(dirname(__FILE__).'/templates');
    $subDomain = preg_match('/.'.$_SERVER['maindomain'].'$/isU', $domain, $matches, PREG_OFFSET_CAPTURE, 0);
    $isFullDomain = !preg_match('/^([a-zA-Z0-9]([-a-zA-Z0-9]{0,61}[a-zA-Z0-9])\.)([a-zA-Z0-9]{1,2}([-a-zA-Z0-9]{0,252}[a-zA-Z0-9])?)\.([a-zA-Z]{2,63})$/isU', $domain, $matches, PREG_OFFSET_CAPTURE, 0);
    if (!$nossl) {
        if (!$subDomain) {
            file_put_contents(
                $nginxFile,
                $templates->render(
                    'nginx-for-ssl-certificate',
                    [
                        'domain' => $domain,
                        'user' => $user,
                    ]
                )
            );
            exec('service nginx force-reload', $output);
            if ($isFullDomain) {
                exec('/root/.acme.sh/acme.sh --issue -d '.$domain.' -d www.'.$domain.' -k ec-384 -w /home/'.$user.'/'.$domain.'/', $output);
            } else {
                exec('/root/.acme.sh/acme.sh --issue -d '.$domain.' -k ec-384 -w /home/'.$user.'/'.$domain.'/', $output);
            }
            exec('mkdir -p /etc/letsencrypt/live/'.$domain, $output);
            exec('/root/.acme.sh/acme.sh --install-cert -d '.$domain.' --ecc \
            --cert-file /etc/letsencrypt/live/'.$domain.'/cert.pem \
            --key-file /etc/letsencrypt/live/'.$domain.'/key.pem \
            --fullchain-file /etc/letsencrypt/live/'.$domain.'/fullchain.pem \
            --ca-file /etc/letsencrypt/live/'.$domain.'/ca.pem \
            --reloadcmd "systemctl restart nginx.service"', $output);
        }
        file_put_contents(
            $nginxFile,
            $templates->render(
                'nginx-maindomain',
                [
                    'domain' => $domain,
                    'user' => $user,
                    'herseUser' => $herseUser,
                    'hersePass' => $hersePass,
                    'subDomain' => $subDomain,
                ]
            )
        );
    } else {
        file_put_contents(
            $nginxFile,
            $templates->render(
                'nginx-nossl',
                [
                    'domain' => $domain,
                    'user' => $user,
                    'herseUser' => $herseUser,
                    'hersePass' => $hersePass,
                    'subDomain' => $subDomain,
                ]
            )
        );
    }
    exec('service nginx force-reload', $output);
}

function removeNginxConfig($domain)
{
    $nginxFile = '/etc/nginx/conf.d/'.$domain.'.conf';
    unlink($nginxFile);
    exec('service nginx force-reload', $output);
}

function createPhpFpmConfig($user)
{
    $phpVersion = str_replace(['php', '-fpm'], '', $_SERVER['phpservice']);
    $phpConfFile = '/etc/php/'.$phpVersion.'/fpm/pool.d/'.$user.'.conf';
    $templates = new League\Plates\Engine(dirname(__FILE__).'/templates');
    file_put_contents($phpConfFile, $templates->render('php-fpm', ['user' => $user]));
    exec('service '.$_SERVER['phpservice'].' reload', $output);
}

function removePhpFpmConfig($user)
{
    $phpVersion = str_replace(['php', '-fpm'], '', $_SERVER['phpservice']);
    $phpConfFile = '/etc/php/'.$phpVersion.'/fpm/pool.d/'.$user.'.conf';
    unlink($phpConfFile);
    exec('service '.$_SERVER['phpservice'].' reload', $output);
}

function copyYesWikiFiles($domain, $user, $dbUser, $herseUser = null, $hersePass = null, $nossl = null)
{
    $tmpFile = '/tmp/yeswiki.zip';
    $destDir = '/home'.'/'.$user.'/'.$domain;
    $sudo = 'sudo -u '.$user.' ';
    exec($sudo.' mkdir -p '.$destDir, $output);
    exec($sudo.' chown '.$user.':'.$user.' -R '.$destDir, $output);
    if (file_exists($tmpFile)) {
        unlink($tmpFile);
    }
    exec($sudo.'curl --insecure -o '.$tmpFile.' '.$_SERVER['source_archive_url']);
    exec($sudo.'unzip '.$tmpFile.' "doryphore/*" -d '.$destDir);
    exec($sudo.'mv '.$destDir.'/doryphore/* '.$destDir.'/');
    exec($sudo.'rm -rf '.$destDir.'/doryphore');
    unlink($tmpFile);
    $herse = '';
    if (!empty($herseUser) && !empty($hersePass)) {
        $herse = $herseUser.':'.$hersePass.'@'; 
    }
    $curl = 'curl -X POST --insecure --data-urlencode \'config[default_language]=fr\' --data-urlencode \'config[wakka_name]='.$domain.'\' --data-urlencode \'config[root_page]=PagePrincipale\' --data-urlencode \'config[mysql_host]=localhost\' --data-urlencode \'config[mysql_database]='.$dbUser['database'].'\' --data-urlencode \'config[mysql_user]='.$dbUser['user'].'\' --data-urlencode \'config[mysql_password]='.$dbUser['password'].'\' --data-urlencode \'config[table_prefix]=yeswiki_\' --data-urlencode \'admin_name='.$_SERVER['admin_id'].'\' --data-urlencode \'admin_password='.$_SERVER['admin_password'].'\' --data-urlencode \'admin_password_conf='.$_SERVER['admin_password'].'\' --data-urlencode \'admin_email='.$_SERVER['admin_email'].'\' --data-urlencode \'config[rewrite_mode]=0\' --data-urlencode \'config[allow_raw_html]=1\' --data-urlencode \'config[allow_robots]=1\' --data-urlencode \'submit=Continue\' \'http'.($nossl ? '' : 's').'://'.$herse.$domain.'/?PagePrincipale&installAction=install\' 2>&1';
    $output = [];
    exec($curl, $output);
    //echo implode("\n", $output);
    return;
}

function checkHerse($herseUser, $hersePass)
{
    if (empty($herseUser) && empty($hersePass)) {
        return false; // no herse needed
    } elseif (empty($herseUser) || empty($hersePass)) {
        throw new Exception('You need an username AND a password to add a herse.');
    }
    return true; // herse needed
}

function editConfigFile() {
    // change fuseau horaire
    // 'contact_mail_func' => 'smtp',
    // 'contact_smtp_host' => 'ssl://mail.gandi.net',
    // 'contact_smtp_port' => '465',
    // 'contact_smtp_user' => 'noreply@yeswiki.pro',
    // 'contact_reply_to' => 'noreply@yeswiki.pro',
    // 'contact_smtp_pass' => 'Ceci est 1 message automatique!',
  
}

function addExtensions() {
    // ferme pour l'option ferme
}

function addStatistics() {
    // requete SQL
    // INSERT INTO `matomo_site` (`idsite`, `name`, `main_url`, `ts_created`, `ecommerce`, `sitesearch`, `sitesearch_keyword_parameters`, `sitesearch_category_parameters`, `timezone`, `currency`, `exclude_unknown_urls`, `excluded_ips`, `excluded_parameters`, `excluded_user_agents`, `excluded_referrers`, `group`, `type`, `keep_url_fragment`, `creator_login`) VALUES (NULL, 'Partage ton outil', 'https://partagetonoutil.fr', '2023-02-01 00:00:00', '0', '1', '', '', 'Europe/Paris', 'EUR', '0', '', '', '', '', '', 'website', '0', 'superadmin'); 
  
}